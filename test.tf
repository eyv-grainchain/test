provider "aws" {
  region     = "us-west-2"
}

resource "aws_vpc" "eyv-vpc" {
  cidr_block           = "172.31.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true
}

resource "aws_subnet" "eyv-public" {
  vpc_id                  = "${aws_vpc.eyv-vpc.id}"
  cidr_block              = "172.31.1.0/24"
  map_public_ip_on_launch = true
}

resource "aws_internet_gateway" "eyv-gw" {
  vpc_id = "${aws_vpc.eyv-vpc.id}"
}

resource "aws_route" "eyv-internet" {
  route_table_id         = "${aws_vpc.eyv-vpc.main_route_table_id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.eyv-gw.id}"
}

resource "aws_eip" "eyv-eip" {
  vpc        = true
  depends_on = ["aws_internet_gateway.eyv-gw"]
}

resource "aws_nat_gateway" "eyv-nat" {
  allocation_id = "${aws_eip.eyv-eip.id}"
  subnet_id     = "${aws_subnet.eyv-public.id}"
  depends_on    = ["aws_internet_gateway.eyv-gw"]
}

resource "aws_route_table" "eyv-routes" {
  vpc_id = "${aws_vpc.eyv-vpc.id}"
}

resource "aws_route" "eyv-route" {
  route_table_id         = "${aws_route_table.eyv-routes.id}"
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = "${aws_nat_gateway.eyv-nat.id}"
}

resource "aws_route_table_association" "eyv-public-association" {
  subnet_id      = "${aws_subnet.eyv-public.id}"
  route_table_id = "${aws_vpc.eyv-vpc.main_route_table_id}"
}

resource "aws_security_group" "eyv-filters" {
  name   = "eyv-filters"
  vpc_id = "${aws_vpc.eyv-vpc.id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_key_pair" "eyv-key" {
  key_name   = "eyv-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDBJaHdH5B/ks+Jd/kU7qZVJU1+8Hl4dR/wy2c/BC9A8085bBfc4QQPr4TqXvtw7lTIevvrRFxN3MVqrAQ7M2/4DQNgNTOBeegbnxqELw612OUDU92GeJAV50MovmnpSJtLt07ujCyvUyOEN29p3k/3bCW69j6FCZGGentNcXLKxK9IkJEI9Huy75hYRvy2TtjCK3fMVgUvqoTFjH6NZKvvWG/ONtst+IWIbC3u9cfW2xVsxK9Fvmn/vRYt8xAyt8KxbAS8sdqBUUF5eBLo/Yj+RwKAOxoet7bgdkVG5FeFuNnzbUCvaim41uY0Do1AiQR7aSyMxjxhsqa8REs/8UeL efren@syavne.duckdns.org"
}

resource "aws_instance" "eyv-test" {
  depends_on      = ["aws_key_pair.eyv-key"]
  ami             = "ami-06f2f779464715dc5"
  instance_type   = "t3a.2xlarge"
  key_name        = "eyv-key"
  subnet_id       = "${aws_subnet.eyv-public.id}"
  root_block_device {
    delete_on_termination = true
    iops                  = 100
    volume_size           = 32
    volume_type           = "gp2"
  }
  vpc_security_group_ids = ["${aws_security_group.eyv-filters.id}"]
}

resource "aws_elb" "eyv-elb" {
  name               = "eyv-elb"
  subnets            = ["${aws_subnet.eyv-public.id}"]

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:80/"
    interval            = 30
  }

  instances                   = ["${aws_instance.eyv-test.id}"]
  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400
}
