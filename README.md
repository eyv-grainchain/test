# Technical evaluation test

Originally, this repository was meant to be uploaded in Bitbucket but the confirmation email never arrived to the address provided.

The objective seems to be the deployment of a single node Kubernetes cluster enabling Istio for the testing of Auth0.

## Environment

- A virtual environment for Python 3.5 was used to get AWS access (library awscli), requirements file in folder aws-env; using 1.16.245 at the time. Credential configuration using default configuration file (~/.aws/credentials).
- Terraform downloaded as zip and made available using the terminal path (export PATH=$PATH:~/test/terraform); using 0.12.9_linux_amd64 at the time. Using awscli default configuration file.
- SSH key provided for access to the EC2 instance (test_rsa.pub).

## Assumptions

Microk8s installed as snap.

Addon istio-injection enabled:
```sh
$ microk8s.kubectl label namespace default istio-injection=enabled
```

SSH user in microk8s group:
```sh
$ sudo usermod -aG microk8s ${USER}
```

Kubernetes configured to use the secret of docker-registry to authenticate with container registries required.
```sh
$ microk8s.kubectl create secret generic regcred --from-file=.dockerconfigjson=config.json --type=kubernetes.io/dockerconfigjson
```
